# JSON formats used in PolarX

This only describes our current setup and is by no means a detailed specification of our expectations. Please review the data and the json structure, but many properties here can be easily removed or not needed. 
The most important properties would be: 

* changes - when is article modified and changed

* section - where is it located 

* components - complete array of storyline'ish object mapped directly to components in framework

* title - title of article

* tags - tags in article

* source - source of article

* authors - who has written

* customProperties - could be any metadata, but depends on content-type and workshop for that

### Example article path (see article.json):
``` 
http://server/polarpaw-api/sport/i/kJXBoA/En-tid-da-folk-dopet-seg-helt-lovlig
``` 
This takes the keyword "/i/xxxxx/" to figure out the ID. And fetches that ID from API.
The article.json format used can easily mapped in our own middleware. All relations and inline objects are requested as separate objects and also included in this single feed.

### Example section path (see section.json):
``` 
http://server/polarpaw-api/sport/
``` 
This just takes the path that extends the current domain and uses that for lookup on the sectionname. 
We also have a separate request for a section endpoint that lists all sections in a newsroom. This is illustrated in sectionlisting.json, but happens behind the scene as a separate request to calculate section of articles. The final product is section.json

### Example tag path (see tag.json):
``` 
http://server/polarpaw-api/sport/tag/tromsoe-il/
``` 
This takes the keyword "/tag/" and makes a lookup on the tagname postfixed.

### Example image path:
``` 
http://server/static/images/b997341b-b8f4-413f-8027-5b6a519300f3?fit=crop&q=80&tight=true&w=680
``` 
This uses the keyword "/static/" to proxy the request to an external microservice hosted by Schibsted. The response is cached in our on premise varnish.